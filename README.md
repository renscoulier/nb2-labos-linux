# Netwerkbeheer 2 - Linux

Naam cursist: NAAM

## Opgave

De bedoeling van dit labo is een klein netwerk op te zetten met een aantal typische services:

- Webserver (LAMP-stack met Apache, PHP, MySQL)
- Fileserver (met Samba)
- DNS (met BIND)
- DHCP

Het netwerk heeft IP range 192.168.15.0/24 en de domeinnaam is `linuxlab.lan`.

We streven er naar om heel de opstelling zoveel mogelijk te automatiseren én voldoende generisch te maken, zodat deze configuratie met een minimum aan moeite kan toegepast worden in een andere situatie.





