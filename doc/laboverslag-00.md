## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. Installeren CentOS7 minimal
==============================
 1. vbox gebruiken voor virtual machine aan te maken ( 512mb ram - 20gb hdd - 2 netwerkkaarten: 1 NAT, 2de host-only aanmaken )
 2. iso mounten als cd, virtual machine opstarten
 3. VM laten opstarten tot je in meer grafische omgeving zit.
 4. a. localization
    - install language op engels laten
    - keyboard instelling wijzigen naar dutch - flamish belgium, deze dan bovenaanzetten en  overige wegdoen
    - tijdzone aanpassen naar Brussels, zeker maken dat klok correct staat
    b. system
    - installation destination uw disk selecteren
 5. begin installation
    - rootpassword ingeven ( Ku.. )
    - gebruiker aanmaken: rens       pw ku...
 6. wachten tot alles gereed is, dan systeem rebooten.

2. IP address instellen op virtual box ( host-machine )
=======================================================
 1. 'host only network' configureren
 -----------------------------------
  - file > preferences >  network > tab 'host-only networks' > nieuwe aanmaken
  - adapter tab: IPv4 192.168.15.1/24
  - dhcp server tab: 
       * check enable server
       * server addres 192.168.15.100 / 24
       * lowerbound: 192.168.15.101 ( IP scope )
       * upper bound: 192.168.15.250

 2. 'host-only network' toepassen voor VM
 ----------------------------------------
  - settings VM > network > tab 'adapter 2' > name aanpassen naar profiel van punt 1

3. IP address controlleren in VM
================================
command 'ip addr' voor netwerkinterfaces te tonen, dan juiste interface zoeken en IP bekijken. Hier werd IP gegeven 192.168.56.

Indien loopback address
------------------------
vi van bestand "/etc/sysconfig/network-scripts/ifcfg-enp0s3" (en op enp0s8 )
onboot aanpassen naar "yes"
netwerkservice rebooten: "sudo systemctl stop network" "sudo systemctl start network"


4. gebruiker aanmaken met admin rights
======================================
 1. inloggen als root gebruiker 
 -------------------------------
  - commando: 'su - root', pw is root pw ( Ku.. )
 2. aanmaken nieuwe gebruiker
 ----------------------------
  - gebruiker aanmaken: 'useradd <username>'
  - wachtwoord toekennen: 'passwd <username>'
 3. admin rechten geven
 ----------------------
  - usermod -G wheel <username>
 extra
 -----
  - gebruikers oplijsten: 'cat /etc/passwd

5. connectie maken van hostsysteem naar VM-host
===============================================
 - terminal openen > command: 'ssh <username>@<ip-VM-host>


### Testplan en -rapport

- het toestel verkrijgt een IP, kan je controlleren met "ip addr" en dan juiste interface bekijken.
- kan via de host-machine een ssh verbinding maken met de virtuele machine.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- aanmaken virtuele machine en opzetten netwerkgegevens

#### Wat ging niet goed?
 
- troubleshooting als er geen IP werd verkregen, maar als je nog nooit met een linux-machine hebt gewerkt is dit al wat lastiger..

#### Wat heb je geleerd?

- efficient googlen op centOS problemen
- dat de logicastructuur van linux wel logisch is, elke instelling kan veranderd worden

#### Waar heb je nog problemen mee?

/

### Referenties

- www.google.be
