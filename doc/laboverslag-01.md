## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. install apache web server
=============================
 1. "sudo yum install httpd" voor installatie te starten, instructies volgen ( altijd 'y' ).
 2. "sudo service httpd start" voor service op te starten
     controlleren of service draait: "service <serviceid> status"
 3. firewall configuratie moet aangepast worden om http verkeer door te laten
   1. "sudo firewall-cmd --list-all" om alle config te tonen die draait op firewall
   2. service toevoegen: "sudo firewall-cmd --add-service <serviceid>", ons geval http

2. install mariaDB ( MYSQL )
============================
 1. "sudo yum install mariaDB"
 2. "sudo systemctl start mariadb.service"
 3. "sudo systemctl enable mariadb.service"
 4. root PW instellen voor db: "sudo /usr/bin/mysql_secure_installation" ( 'y' op alles voor gemakkelijkheid )

3. install PHP
===============
 1. "sudo yum install php"
 2. http service restarten ( "sudo service http restart" )

4. php script aanmaken voor test
=================================
 1. "sudo nano /var/www/html/info.php"
   daarin dan scriptje maken:
   <?php
   phpinfo();
   ?>

### Testplan en -rapport

1. testen of apache correct geinstalleerd is
--------------------------------------------
  - in browser surfen naar hostname van systeem, zou dan webpagina moeten krijgen van: "testing 1-2-3"

2. testen of php goed is geinstalleerd
---------------------------------------
 - uitvoeren van script "http://192.168.15.101/info.php"

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- installatie apache, kan eigenlijk niet veel mis lopen

#### Wat ging niet goed?

- uitzoeken waarom hij niet wou http verbinding leggen naar host adres, uiteindelijk bleek firewall toe stond, wat moeilijk te weten is.
- installatie php, had specifieke module geinstalleerd, niet de gehele

#### Wat heb je geleerd?

- check firewall bij installatie apache 
- google is your friend

#### Waar heb je nog problemen mee?

troubleshooting, maar ligt puur aan de ervaring die er nog niet is.

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
