## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. Ansible role bertvv.samba afhalen en plaatsen in ansible/roles
2. in host_vars srv002.yml aanmaken en configuratie in plaatsen:
   - el7_install_packages
   - samba_netbios_name
   - el7_user_groups
   - el7_users
   - samba_share_root
   - samba_shares
   - samba_users
   - samba_load_homes
   - samba_load_printers
3. in ansible/site.yml aanmaken 
   - hosts: srv002
   - sudo: true
   - roles:
      - bertvv.el7
      - bertvv.samba
4. lamp/vagrant_hosts.yml server toevoegen

### Testplan en -rapport

test-bat laten lopen en alle tests slagen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- algemene rol installeren en deze beschikbaar maken

#### Wat ging niet goed?

Veel zitten try-and-error voor alle waarden in de hosts_vars correct te plaatsen.

#### Wat heb je geleerd?

Alles 2x lezen en alles 10x opslaan voor er een provision moet gebeuren :D

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
