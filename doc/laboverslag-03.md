## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. ansible role afhalen bertvv.bind
2. in site.yml waardes toevoegen voor srv003
3. in host_vars srv003.yml aanmaken
   - bind_zone_name
   - bind_zone_networks
   - bind_zone_name_servers
   - bind_zone_mail_servers
   - bind_listen_ipv4
   - bind_listen_ipv6
   - bind_allow_query
   - bind_recursion
   - bind_zone_hostmaster_email
   - bind_zone_ttl
   - bind_zone_time_to_refresh
   - bind_zone_time_to_retry
   - bind_zone_time_to_expire
   - bind_zone_minimum_ttl
   - bind_zone_hosts


### Testplan en -rapport

Licht gedetailleerd toe hoe je aantoont dat de specificaties, zoals in de labo-opgave opgegeven, ook effectief voldaan zijn. Voer deze stappen ook uit en geef telkens aan wat het resultaat is (aan de hand van console-uitvoer of een schermafbeelding). Wanneer je je labo mondeling toelicht, volg je dit testplan en toon je de resultaten.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

roles installeren en basis config

#### Wat ging niet goed?

alle puntjes op de I plaatsen

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
