## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. Stap 1
2. Stap 2
3. enz.

### Testplan en -rapport

Licht gedetailleerd toe hoe je aantoont dat de specificaties, zoals in de labo-opgave opgegeven, ook effectief voldaan zijn. Voer deze stappen ook uit en geef telkens aan wat het resultaat is (aan de hand van console-uitvoer of een schermafbeelding). Wanneer je je labo mondeling toelicht, volg je dit testplan en toon je de resultaten.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- ...

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
