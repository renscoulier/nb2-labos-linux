<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{ wordpress_db }}');

/** MySQL database username */
define('DB_USER', '{{ wordpress_user }}');

/** MySQL database password */
define('DB_PASSWORD', '{{ wordpress_pw }}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' 7j`ppWDwM<lY4T{DSpl&!27|IeAZLT$TB%/k&Hg o-@U6-98*m.%FK:^~!M-O~|');
define('SECURE_AUTH_KEY',  'bP4c*x%?lvP<END:Cj;Xd]iqUu%<3pct}0@A{0MCM6_W4_Hq5qJ2fg^_v-2x,TUx');
define('LOGGED_IN_KEY',    '|J_Vu &UyumLC)Re7@~4,0A}(^)]w`:ly+BBf)V4s kHsW0?bt+1<BF.Y-J/fzsf');
define('NONCE_KEY',        '3o!?5B IgT/y>$PVDj4R=!y6Qz[|GNAly{}Z:Z`O]8r(d/A!=:DwpnwId{,mAU4U');
define('AUTH_SALT',        'NwD[-1Ml7]J~[5DDa=}wNn94WY&pMU]~FML_XY27At]FxeNI{1a]x_Y9#I}]Dd~9');
define('SECURE_AUTH_SALT', 'x.8lpw-hZr]c5,P~-=ZRLJ8r*Nuv9)B*q_%4E2!g-(6>Clinr@o%*as<E+Qk}y9}');
define('LOGGED_IN_SALT',   '+?O Hg5<jaqYk4Gb-Pl.a^^LyLb|V>WmAALzLG|zE+HZ(3`1B|C9il;.~V*x+W6|');
define('NONCE_SALT',       ' BnxV8A,&-MQ|]mYr-/][B(O1WSK99EZ~g/:YD&Cu6u{Jnu:P65|!JBCk0;z=uth');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * See http://make.wordpress.org/core/2013/10/25/the-definitive-guide-to-disabling-auto-updates-in-wordpress-3-7
 */

/* Disable all file change, as RPM base installation are read-only */
define('DISALLOW_FILE_MODS', true);

/* Disable automatic updater, in case you want to allow
   above FILE_MODS for plugins, themes, ... */
define('AUTOMATIC_UPDATER_DISABLED', true);

/* Core update is always disabled, WP_AUTO_UPDATE_CORE value is ignore */

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', '/usr/share/wordpress');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
