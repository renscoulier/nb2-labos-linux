## Laboverslag: TITEL

Naam cursist: Rens Coulier
URL Bitbucket repo: IR
Bitbucket repo: https://bitbucket.org/renscoulier/

### Procedures

Documenteer gedetailleerd de werkwijze die je gevolgd hebt. Het moet mogelijk zijn om aan de hand van dit verslag, en zonder het gebruik van externe bronnen, het volledige labo te reproduceren. Vergeet niet te verwijzen naar de informatiebronnen die je gebruikt hebt en die achteraan het document op te sommen.

1. Vagrant
--------------------
 - download vagrant van: https://www.vagrantup.com/downloads.html
 - walkthrough handleiding van vagrant: http://docs.vagrantup.com/v2/getting-started/   index.html

 1. project setup
 -----------------
  - maak werkfolder op het hostsysteem
  - maak vargant file aan met het commando "vagrant init"

 2. box: basis image
 -------------------
  - voeg een vagrant box ( basis image ) toe via het commando "vagrant box add <locatie> --name <naamBox>"
    locatie = plaats van waar de image wordt afgehaald, kan server of lokaal zijn
    In ons geval: "http://netlabsrv/public/Software/VM/centos70-nocm-1.0.10.box"
    name = hoe hij lokaal zal worden opgeslagen
  - voor de box te koppelen aan vagrant moet er een waarde in de vagrant file worden aangepast. aan te passen regel: "config.vm.box = <naamBox>"

 3. virtuele machine installeren + connectie maken van hostsysteem
 ------------------------------------------------------------------
  - virtuele machine kan geinstalleerd worden door de vagrant file te laten inlezen door vagrant. Commando hiervoor is "vagrant up" vanuit directory waar de vagrant file te vinden is. ( verwijderen = "vagrant destroy"
  - 




- vagrant file koppelen met vagrant_hosts.yml
- ansible/site.yml in root van vagrant locatie plaatsen




































### Testplan en -rapport

Licht gedetailleerd toe hoe je aantoont dat de specificaties, zoals in de labo-opgave opgegeven, ook effectief voldaan zijn. Voer deze stappen ook uit en geef telkens aan wat het resultaat is (aan de hand van console-uitvoer of een schermafbeelding). Wanneer je je labo mondeling toelicht, volg je dit testplan en toon je de resultaten.

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- ...

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

#### Wat heb je geleerd?

Ga ook wat verder kijken dan de voor de hand liggende zaken.

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
