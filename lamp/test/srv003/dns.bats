#! /usr/bin/env bats
#
# Author:   Bert Van Vreckem <bert.vanvreckem@gmail.com>
#
# Test a DNS server

domain=linuxlab.lan
reverse_domain=56.168.192.in-addr.arpa

ns_ip=192.168.56.12

#{{{ Helper functions

# Perform a forward lookup
# Usage: forward_lookup HOSTNAME EXPECTED_IP
forward_lookup() {
  result="$(dig @${ns_ip} $1.${domain} +short)"
  expected=$2
  [ "${expected}" = "${result}" ]
}

# Perform a forward lookup with aliases
# Usage: alias_lookup ALIAS EXPECTED_HOSTNAME EXPECTED_IP
alias_lookup() {
  result="$(dig @${ns_ip} $1.${domain} +short)"
  expected_hostname="${2}.${domain}."
  expected_ip=$3
  echo ${result} | grep ${expected_ip}
  echo ${result} | grep ${expected_hostname}
}

# Perform a reverse lookup
# Usage: reverse_lookup IP EXPECTED_HOSTNAME
reverse_lookup() {
  result="$(dig @${ns_ip} -x ${1} +short)"
  expected="${2}.${domain}."
  [ "${expected}" = "${result}" ]
}

#}}}

#{{{ Basic white box tests
@test 'DNS service should be running' {
  sudo systemctl status named.service
}

@test 'The `dig` command should be installed' {
  which dig
}

@test 'Named.conf should be syntactically correct' {
  sudo named-checkconf /etc/named.conf
}

@test 'Forward lookup zone should be syntactically correct ' {
  sudo named-checkzone ${domain} /var/named/${domain}
}

@test 'Reverse lookup zone should be syntactically correct ' {
  sudo named-checkzone ${reverse_domain} /var/named/${reverse_domain}
}
#}}}

@test 'It should return the NS record(s)' {
  result="$(dig @${ns_ip} ${domain} NS +short)"
  [ -n "${result}" ] # The result should not be empty
}

@test 'It should be able to resolve host names' {
  forward_lookup srv010 192.168.56.10 # DNS master
  forward_lookup srv011 192.168.56.11 # DNS slave
  forward_lookup srv001 192.168.56.15 # web server
  forward_lookup srv002 192.168.56.20 # Fileserver
  forward_lookup srv003 192.168.56.12 # DHCP
  forward_lookup srv020 192.168.56.21 # Mail
  forward_lookup srv021 192.168.56.16 # Intranet
}

@test 'It should be able to do reverse lookups' {
  reverse_lookup 192.168.56.10 srv010 # DNS master
  reverse_lookup 192.168.56.11 srv011 # DNS slave
  reverse_lookup 192.168.56.15 srv001 # web server
  reverse_lookup 192.168.56.20 srv002 # Fileserver
  reverse_lookup 192.168.56.12 srv003 # DHCP
  reverse_lookup 192.168.56.21 srv020 # Mail
  reverse_lookup 192.168.56.16 srv021 # Intranet
}

@test 'It should be able to resolve aliases' {
  alias_lookup ns1      srv010 192.168.56.10 # DNS master
  alias_lookup ns2      srv011 192.168.56.11 # DNS slave
  alias_lookup www      srv001 192.168.56.15 # web server
  alias_lookup file     srv002 192.168.56.20 # Fileserver
  alias_lookup dhcp     srv003 192.168.56.12 # DHCP
  alias_lookup mail     srv020 192.168.56.21 # Mail
  alias_lookup smtp     srv020 192.168.56.21 # Mail
  alias_lookup imap     srv020 192.168.56.21 # Mail
  alias_lookup intra    srv021 192.168.56.16 # Intranet
  alias_lookup intranet srv021 192.168.56.16 # Intranet
}

@test 'It should return the MX record(s)' {
  result="$(dig @${ns_ip} ${domain} MX +short)"
  expected="10 srv020.${domain}."

  [ "${expected}" = "${result}" ]
}
